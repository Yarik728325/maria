import React, { useEffect } from "react";
import { dataStartRequest } from "../../redux/actions/dataRequest";
import { useDispatch, useSelector } from "react-redux";
import ItemsList from "../../components/itemsList";
import PaginationRounded from "../../components/pagination";
import SearchItems from "../../components/searchItems";
import './style.scss';

const Home = ()=>{
  const  { page,search,results} = useSelector((state) => state.dataApi);
  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(dataStartRequest({page,search}));
  },[dispatch,page,search])
  const tmpComponent = !results ?<p className="time">Loading</p> : <ItemsList results={results} />
  return (
    <>
     <SearchItems/>
     {tmpComponent}
     <PaginationRounded/>
    </>
  )
}

export default Home;