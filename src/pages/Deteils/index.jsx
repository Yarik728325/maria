import React, { memo } from "react";
import { useSelector } from "react-redux";
import './style.scss';


const Deteils = () => {
  const { currentUser } = useSelector(state=>state.dataApi);
  const {name, height, mass, hair_color, skin_color} = currentUser;
  return (
    <>
      <div className="detailsWrapper">
        <p><b>name :</b>{name}</p>
        <p><b>height :</b>{height}</p>
        <p><b>mass :</b>{mass}</p>
        <p><b>hair_color :</b>{hair_color}</p>
        <p><b>skin_color :</b>{skin_color}</p>
      </div>
    </>
  )
}

export default memo(Deteils);