import React from "react";
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { searchNameAction } from "../../redux/actions/dataRequest";
import { useDispatch,useSelector } from "react-redux";;

const SearchItems = () => {
  const { search } = useSelector(state=>state.dataApi)
  const dispatch = useDispatch(); 
  const changeName = (e)=>{
    dispatch(searchNameAction({
      page:1,
      search:e.target.value
    }))
  }
  return(
  <Box
    component="form"
    sx={{
      '& > :not(style)': { m: 1, width: '25ch' },
    }}
    noValidate
    autoComplete="off"
  >
    <TextField id="standard-basic" label="searchName" variant="standard" onChange={(e)=>changeName(e)} value={search} />
  </Box>
  )
}

export default SearchItems;