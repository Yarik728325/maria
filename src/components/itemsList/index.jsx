import React from "react";
import ItemsDeteils from "../itemsDeteils";
import './style.scss'

const ItemsList = ({ results }) =>{
  results =  results.map(e=>{
    const { birth_year, eye_color, gender, hair_color, mass, name, skin_color  } = e;
    const id = e.url.replaceAll(/\D/g,'');
    const url = `/people/${id}`;
    return <ItemsDeteils
      
      birth_year = {birth_year}
      eye_color = { eye_color}
      gender = { gender }
      hair_color = { hair_color }
      mass = { mass }
      name = { name }   
      skin_color = { skin_color }
      key={Math.random().toString(36).substring(7)}
      url = {url}
    />
  })
 return <div className="wrapperLists" >{ results }</div>
}

export default ItemsList;