import React, { memo } from "react";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { getDetailsUser } from "../../redux/actions/dataRequest";
import './style.scss';
import { useDispatch } from "react-redux";

const ItemsDeteils = ({birth_year, eye_color, gender, hair_color, mass, name, skin_color,url }) => {
  const dispatch = useDispatch();
  return(
    <Card sx={{ maxWidth: 345 }} onClick={()=>{
      dispatch(getDetailsUser(url))
    }} >
      <CardActionArea>
        <CardMedia
          component="img"
          height="140"
          image="https://www.liveabout.com/thmb/j1QL7IsAy-pXyr37_yUjD0JQpUY=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/EP2-IA-32565_R_8x10-56a83bdc5f9b58b7d0f1b389.jpg"
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {name}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <div className="about"><b>Color eye: </b> {eye_color}</div>
            <div className="about"><b>Gender: </b> {gender}</div>
            <div className="about"><b>Color hair: </b> {hair_color}</div>
            <div className="about"><b>Mass: </b> {mass}</div>
            <div className="about"><b>Color Skin: </b> {skin_color}</div>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default memo(ItemsDeteils);
