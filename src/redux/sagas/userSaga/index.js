import { call, takeLatest, put } from "@redux-saga/core/effects";
import 
{ 
  dataStartRequest, 
  dataSeccessRequest,
  getDetailsUser,
  getDetailsUserSuccess
} from "../../actions/dataRequest";
import { getData } from "../../../api";
import history from "../../../utils/routing";

export  function* getAllUser({payload}){
  const { page,search } = payload;
  const data = yield call(getData, `https://swapi.dev/api/people?page=${page}&search=${search}`);
  yield put(dataSeccessRequest({result:data.results,count:data.count}));
} 
export function*  getOneUser({payload}){
  const data = yield call(getData,`https://swapi.dev/api${payload}`);
  yield put(getDetailsUserSuccess(data));
  history.push(payload);
}

export default function* watcherUser() {
  yield takeLatest(dataStartRequest.type, getAllUser);
  yield takeLatest(getDetailsUser.type, getOneUser);
}
