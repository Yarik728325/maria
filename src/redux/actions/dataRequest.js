import { createAction } from "../../utils/createAction";

export const dataStartRequest = createAction('Actions/dataStartRequest/');
export const dataSeccessRequest = createAction('Actions/dataSuccessRequest/');
export const changePaginationPage = createAction('Actions/Pagination/Change/');
export const searchNameAction = createAction('Actions/SearchName/');
export const getDetailsUser = createAction('Actions/GetDetails/User/');
export const getDetailsUserSuccess = createAction('Actions/GetDetails/Success/');