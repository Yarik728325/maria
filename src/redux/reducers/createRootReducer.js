import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import dataApi from './dataApi';

// eslint-disable-next-line import/no-anonymous-default-export
export default (history) => combineReducers({
  dataApi,
  router: connectRouter(history),
});
