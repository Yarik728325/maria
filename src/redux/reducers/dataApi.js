import { 
  dataSeccessRequest,
  dataStartRequest,
  changePaginationPage,
  searchNameAction,
  getDetailsUser,
  getDetailsUserSuccess,
} from "../actions/dataRequest";

const initinalState = {
  count:null,
  results : null,
  error:false,
  page:1,
  search:'',
  currentUser:null,
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initinalState, { type, payload }) => {
  switch (type) {
    case getDetailsUserSuccess.type:
      return{
        ...state,
        currentUser:payload
      }
    case getDetailsUser.type:
      return{
        ...state,
        currentUser:null
      }
    case dataStartRequest.type:
      return{
        ...state,
        results:null,
      }
    case searchNameAction.type:
      return{
        ...state,
        page:payload.page,
        search:payload.search,
      }
    case dataSeccessRequest.type:
      return{
        ...state,
        loading:false,
        results:payload.result,
        count:payload.count,
      }
    case changePaginationPage.type:
      return{
        ...state,
        page:payload,
      }
    default:
      return state;
  }
};