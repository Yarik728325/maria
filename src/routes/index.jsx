import React from 'react';
import { Route, Router } from 'react-router';
import history from '../utils/routing';
import Home from '../pages/Home';
import Deteils from '../pages/Deteils';

const routes = [
  {
    id: 'main',
    path: '/',
    exact: true,
    component: Home,
  },
  {
    id:'details',
    path:'/people/:id',
    exact:true,
    component:Deteils
  }
];

const RouterSwitch = () => {
  return (
    <Router history={history}>
      {routes.map((e) => {
        const { id, ...props } = e;
        return <Route key={id} {...props} />;
      })}
    </Router>
  );
};

export default RouterSwitch;
