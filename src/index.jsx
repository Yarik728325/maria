import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { PersistGate } from 'redux-persist/integration/react';
import { createBrowserHistory } from 'history';
import 'regenerator-runtime/runtime';
import createStore from './redux/createStore';
import App from './components/app';

const history = createBrowserHistory();
const { store, persistor } = createStore(history);

ReactDOM.render(
  <Provider store={store}>
       <PersistGate persistor={persistor}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </PersistGate>
  </Provider>,
  document.getElementById('root'),
);
